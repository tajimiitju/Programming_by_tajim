clc;
clear all;
close all;

f1 = input('Enter the equation:','s');
f=inline(f1);
a=input('Enter the 1st boundary value: ');
b=input('Enter the 2nd boundary value: ');
t=input('inter the tolerance= ');
for i=1:10
    fprintf('\n Root lies between (%.5f,%.5f)',a,b);
    x(i)=(a*f(b)-b*f(a))/(f(b)-f(a));
    if f(x(i))>0
        b=x(i);
    else
        a=x(i);
    end
    
    fprintf('\n Therefore, x%d=%.5f\nHere, f(x%d)=%.4f',i,x(i),i,f(x(i)));
    p=x(i);
end
for j=1:i
    error(j)=p-x(j);
end
ans=p;
plot(error);
grid on;
title('Plot of error');
xlabel('iteration');
ylabel('error');