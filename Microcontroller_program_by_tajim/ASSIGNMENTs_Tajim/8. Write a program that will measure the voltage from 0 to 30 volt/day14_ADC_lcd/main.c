/*
 * day_15_adc_all_2.56V
 *
 * Created: 10/24/2017 7:45:08 PM
 * Author : User
 */ 
#define F_CPU 8000000UL
#include <avr/io.h>
#include <stdio.h>
#include <util/delay.h>
#include "lcd.h"
#include "adc.h" 
char line[16];
//int adc_value = 0;

float adc_value = 0;
float adc_volt = 0;
float v = 0;


int main(void)
{
	LCDInit(); // declare lcd data port and control port
    /* Replace with your application code */
    while (1) 
    {
		LCDInit(); //Initialize LCD Display
		adc_init();//Function call
		
		sprintf(line,"ADC Value:");
		LCDGotoXY(1,1);
		LCDString(line);
		
		sprintf(line,"ADC Volt:");
		LCDGotoXY(1,2);
		LCDString(line);
		
		
		while(1)
		{
			adc_value = adc_read(0); //ADC Read
			//adc_volt = ((adc_value*5)/1024.0); ref =5v
			adc_volt = ((adc_value*2.56)/1024.0); //ref =2.56v
			v = adc_volt*12.195; //as we take r1=56k and r2=5k 
			
			sprintf(line,"%.2f", adc_value);
			LCDGotoXY(11,1);
			LCDString(line);
			
			sprintf(line,"%.2f", v);
			LCDGotoXY(11,2);
			LCDString(line);
			
			_delay_ms(50);
		}

		
    }
}

